const { request, response } = require("express");


const validateFileUpload = (req = request, res = response, next) => {
    if (!req.files || Object.keys(req.files).length === 0 || !req.files.file) {
        return res.status(400).json({
            msg: 'No exits file'
        })
    }
    next();
}

const permittedExt = (req = request, res = response, next) => {

    const extensValidas = ['jpg', 'png', 'jpeg', 'git']

    const { name } = req.files.file;
    let [a, b] = name.split('.')

    if (!extensValidas.includes(b)) {
        return res.status(400).json({
            msg: `'${b}', no es una extension valida, use: ${extensValidas}`
        })
    }
    // console.log(req.files.file);

    next();
}


module.exports = {
    validateFileUpload,
    permittedExt
}