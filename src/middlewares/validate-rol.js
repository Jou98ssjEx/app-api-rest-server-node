const { request, response } = require("express");


const isAdminRol = (req = request, res = response, next) => {

    // validar al usuario que este en el req
    // validate the user who is in the req
    if (!req.userAuth) {
        res.status(500).json({
            msg: 'It is required to validate the role without the token first.'
        });
    }

    const { rol, name } = req.userAuth;

    // validar el rol del usuario
    // validate user role
    if (rol !== 'ADMIN_ROLE') {
        res.status(500).json({
            msg: `The user: ${name} is not authorized for this`
        })
    }
    next();
}

const hasRol = (...roles) => {

    return (req = request, res = response, next) => {

        // validar al usuario que este en el req
        // validate the user who is in the req
        if (!req.userAuth) {
            return res.status(500).json({
                msg: 'It is required to validate the role without the token first.'
            });
        }

        // Validar si incluye estos rol
        // Validate if it includes these roles
        console.log(req.userAuth.rol);
        if (!roles.includes(req.userAuth.rol)) {
            return res.status(401).json({
                msg: `The service requires one of these roles: ${roles}`
            })
        }

        next();

    }
}

module.exports = {
    isAdminRol,
    hasRol
}