console.log(' ');
// Se requiere variables de entornos
// Environment variables are required
require('dotenv').config();

const Server = require('./server/server');

// Creando el servidor
// Creating the server
const server = new Server();

// Escuchando al servidor
// Listening to the server
server.listener();