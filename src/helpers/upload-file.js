const path = require('path');
const { v4: uuid } = require('uuid');

const upload_Files = (files, extensValidas = ['jpg', 'png', 'jpeg', 'git'], folder = '') => {
    return new Promise((resolve, reject) => {

        const { file } = files;

        const nameBreak = file.name.split('.');
        const extension = nameBreak[nameBreak.length - 1];

        // validar la extendion
        if (!extensValidas.includes(extension)) {
            reject(`Extension: '${extension}' no es validad, use: ${extensValidas}`)
            return
        }

        const nameTemp = uuid() + '.' + extension;

        const uploadPath = path.join(__dirname, '../upload', folder, nameTemp);

        file.mv(uploadPath, (error) => {
            if (error) {
                reject(error)
                return
            }

            resolve(nameTemp);

        })

    })
}


module.exports = {
    upload_Files
}