// Clase para inicializar los sms
class Message {
    constructor(uid, name, message) {
        this.uid = uid;
        this.name = name;
        this.message = message;
    }
}


class ChatMessages {
    constructor() {
        this.messages = [];
        this.users = {};
    }

    // get mostrar los ultimos 10 mensajes
    get lastUser10() {
        this.messages = this.messages.splice(0, 10);
        return this.messages;
    }

    // get para convertir el objeto en un array de usuarios
    get userArr() {
        // convierte el objeto en un arry de objetos : [ { }, { }, { }]
        return Object.values(this.users);
    }

    //Enviar los mensajes metodo
    sendMessage(uid, name, message) {

        // Agregar al array de mensaje, el nuevo mensaje
        this.messages.unshift(new Message(uid, name, message));

    }

    // Usuario Conectado
    userConnect(user) {
        //del objeto de usuario se le agrega un usuario y tomo la llave como id
        this.users[user.id] = user;
    }

    // Usuario Desconectado
    userDisconnect(id) {
        // delete: eliminar una propiedad de un objeto literal
        delete this.users[id];

    }
}

module.exports = ChatMessages;