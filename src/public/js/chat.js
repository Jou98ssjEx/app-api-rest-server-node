const url = (window.location.hostname.includes('localhost')) ?
    'http://localhost:3000/api/auth/' :
    'https://heroku';

let userAuth, socket;

// Referencias html
const txtUid = document.querySelector('#txtUid');
const txtMessage = document.querySelector('#txtMessage');
const ulUsers = document.querySelector('#ulUsers');
const ulMessage = document.querySelector('#ulMessage');
const btnExit = document.querySelector('#btnExit');


const validateToken = async() => {

    const token = localStorage.getItem('token') || '';
    // console.log({ token });
    // validar el token
    if (token.length <= 10) {
        window.location = 'index.html'
        throw new Error('Token Incorrect');
    }

    // Peticion para validar token
    const resp = await fetch(url, {
        headers: {
            'x-token': token
        }
    });

    // console.log({ resp });

    // Manejar la resp en json()
    const { userAuth: user, token: tokenDB } = await resp.json();
    // console.log({ user }, { tokenDB });

    localStorage.setItem('token', tokenDB);

    userAuth = user;

    document.title = userAuth.name;

    await socketConnect();


}


const socketConnect = async() => {
    // Referencia del socket en el frontend
    // vamos a enviar el token por los headers del socket
    socket = io({
        extraHeaders: {
            'x-token': localStorage.getItem('token')
        }
    });

    // Coneccion de usuario
    socket.on('connect', () => {
        console.log('User connect');
    })

    // Desconeccion de usuario
    socket.on('disconnect', (payload) => {
        console.log('User disconnect');
        console.log(payload);
    })

    // Ecuchar los mensaje que se reciben
    socket.on('listen-message', drawMessage)
        // socket.on('listen-message', (payload) => {
        //     console.log(payload);
        // })

    // Escuchar los usuarios activos
    socket.on('user-active', drawUserList)
        // socket.on('user-active', (payload) => {
        //     console.log(payload);
        // })

    // Escuchar los mensajes privados
    socket.on('private-message', (payload) => {
        console.log('Private: ', payload);
    })
}


const drawUserList = (users = []) => {
    let listHtml = ``;

    users.forEach(({ name, uid }) => {
        listHtml += `
        <li>
            <p>
                <h5 class="text-success"> ${name} </h5>
                <span class="f-6 text-muted"> ${uid} </span>
            </p>
        </li>
        `;
    })

    // ulUsers.innerHTML = listHtml;
    ulUsers.innerHTML = listHtml;
}

const drawMessage = (messages = []) => {
    let messageListHtml = ``;
    messages.forEach(({ name, message }) => {
        messageListHtml += `
    <li>
        <p>
            <span class="text-info"> ${name} </span>
            <span class=""> ${message} </span>
        </p>
    </li>
    `;
    })

    ulMessage.innerHTML = messageListHtml;
}


txtMessage.addEventListener('keyup', ({ keyCode }) => {
    // console.log(ev);

    // variable sms, uid
    const message = txtMessage.value;
    const uid = txtUid.value;

    // si el keyCOde es diferente de 13: return
    if (keyCode !== 13) return

    // 
    if (message.length === 0) return

    // Emitir el sms al back
    socket.emit('send-message', { uid, message });

    txtMessage.value = '';
});

const init = async() => {

    await validateToken();
}


init();

// // Referencia del socket en el frontend
// const socket = io();

// socket.on('connect', () => {
//     console.log('Connect-Front');
// })