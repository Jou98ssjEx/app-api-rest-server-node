const { Schema, model } = require('mongoose');


const CategorySchema = Schema({
    name: {
        type: String,
        required: [true, ' the name is required'],
        unique: true
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        requierd: true
    }
});


CategorySchema.methods.toJSON = function() {
    const { __v, _id, status, ...category } = this.toObject();

    category.uid = _id;

    return category
}


module.exports = model('Category', CategorySchema);