const CategoryModel = require('./category.model');
const ProductModel = require('./product.model');
const RolModel = require('./rol.model');
const UserModel = require('./user.model');

module.exports = {
    CategoryModel,
    ProductModel,
    RolModel,
    UserModel
}