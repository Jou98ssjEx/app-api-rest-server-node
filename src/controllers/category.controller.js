const { request, response } = require("express");
const { CategoryModel } = require("../models");

const getCategories = async(req = request, res = response) => {

    const { limit = 5, desde = 0 } = req.query;
    const query = { status: true };

    // Numero de categorias y catergorias
    const [count, categories] = await Promise.all([
        CategoryModel.countDocuments(query),
        CategoryModel.find(query)
        .populate('user', 'name')
        .skip(Number(desde))
        .limit(Number(limit))
    ]);


    res.status(200).json({
        msg: 'getCategories',
        count,
        categories
    });
}

const getCategoryById = async(req = request, res = response) => {

    const { id } = req.params;

    const category = await CategoryModel.findById(id)
        .populate('user', 'name');

    if (!category.status) {
        return res.status(401).json({
            msg: 'La categoria no existe'
        })
    }

    res.json({
        msg: 'getCategoryId',
        category
    })
}

const createCategory = async(req = request, res = response) => {

    const name = req.body.name.toUpperCase();

    const categoryFound = await CategoryModel.findOne({ name });

    try {
        if (categoryFound) {
            res.status(401).json({
                msg: `La categoria: ${categoryFound.name} ya existe!`
            });
        }

        const data = {
            name,
            user: req.userAuth._id
        }

        let category = new CategoryModel(data);

        await category.save()

        res.json({
            msg: 'createCategory',
            category

        });

    } catch (error) {
        res.status(500).json({
            msg: 'Error al crear una categoria',
            error
        })
    }

}

const updateCategory = async(req = request, res = response) => {

    const { id } = req.params;
    const { status, name, ...data } = req.body;

    data.name = name.toUpperCase();
    data.user = req.userAuth.id;

    const category = await CategoryModel.findOne({ name: data.name });

    if (category) {
        return res.status(401).json({
            msg: `La categoria: ${category.name} ya existe`
        });
    }

    const newCategory = await CategoryModel.findByIdAndUpdate(id, data, {
        new: true
    }).populate('user', 'name');

    res.json({
        msg: 'updateCategory',
        newCategory
    })
}

const deleteCategory = async(req = request, res = response) => {

    const { id } = req.params;

    const CategoryFound = await CategoryModel.findById(id);

    if (!CategoryFound.status) {
        return res.status(401).json({
            msg: `La categoria no existe`
        })
    }

    const category = await CategoryModel.findByIdAndUpdate(id, {
        status: false
    }, { new: true });


    res.json({
        msg: 'deleteCategory',
        category
    })
}

module.exports = {
    getCategories,
    getCategoryById,
    createCategory,
    updateCategory,
    deleteCategory
}