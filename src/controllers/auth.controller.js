const { request, response } = require("express");
const bcryptjs = require('bcryptjs');

const { UserModel } = require('../models');
const { generarToken, verify } = require('../helpers');

const login = async(req = request, res = response) => {

    try {

        const { email, password } = req.body;

        const user = await UserModel.findOne({ email });

        // Si el email existe
        // If the email exists
        if (!user) {
            res.status(400).json({
                msg: 'Email and Password incorrect - email'
            });
        }

        // Si el usuario esta activo
        // If the user is active
        if (!user.status) {
            res.status(400).json({
                msg: 'Email and Password incorrect - status'
            });
        }

        // Si la password hace macth
        const validatePass = bcryptjs.compareSync(password, user.password);

        if (!validatePass) {
            res.status(400).json({
                msg: 'Email and Password incorrect - pass'
            });
        }

        // generar JWT

        const token = await generarToken(user.id);


        res.json({
            msg: 'Login ok',
            user,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en el Servidor',
            error
        })
    }


}

const googleSingIn = async(req = request, res = response) => {

    const { token_google } = req.body;
    // console.log(token_google);
    try {
        const { email, name, img } = await verify(token_google);

        let user = await UserModel.findOne({ email });
        console.log(user);

        if (!user) {
            const data = {
                name,
                email,
                img,
                password: ':P',
                google: true,
                rol: 'USER_ROLE'
            }

            user = new UserModel(data);
            await user.save();
        }

        // Si el user tiene stado en false
        // If the user's status is set to false
        if (!user.status) {
            return res.status(401).json({
                msg: ' User esta desabilitado'
            })
        }

        // generar JWT

        const token = await generarToken(user.id);

        res.json({
            msg: ' Todo Ok',
            user,
            token
            // token_google
        });

    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'no se pudo verificar el token',
            error
        })
    }


}

const renewToken = async(req = request, res = response) => {
    const { userAuth } = req;

    // Usuario autenticado : userAuth 
    const token = await generarToken(userAuth.id);

    res.json({
        userAuth,
        // jo: 'fd'
        token

    })
}



module.exports = {
    login,
    googleSingIn,
    renewToken
}