const { request, response } = require("express");

const { ProductModel } = require('../models');

const getProducts = async(req = request, res = response) => {

    const { desde = 0, limit = 5 } = req.query;
    const query = { status: true };

    const [count, products] = await Promise.all([
        ProductModel.countDocuments(query),
        ProductModel.find(query)
        .populate('user', 'name')
        .populate('category', 'name')
        .skip(Number(desde))
        .limit(Number(limit))
    ]);

    res.json({
        msg: 'getProducts',
        count,
        products
    });
}

const getProductById = async(req = request, res = response) => {

    const { id } = req.params;

    const product = await ProductModel.findById(id)
        .populate('user', 'name')
        .populate('category', 'name');

    if (!product) {
        res.status(401).json({
            msg: 'El producto no existe'
        });
    }

    // estado del producto
    if (!product.status) {
        res.status(401).json({
            msg: 'El producto no existe - status'
        })
    }

    res.json({
        msg: 'getProductById',
        product
    });
}

const createProduct = async(req = request, res = response) => {

    try {
        // Sacar el body
        const { user, ...body } = req.body;

        // verificar si el producto exite
        const existProduct = await ProductModel.findOne({ name: body.name.toUpperCase() });
        if (existProduct) {
            return res.status(401).json({
                msg: `El  porducto: ${body.name.toUpperCase()} ya existe`
            })
        }

        // verificar si el categoria exite

        // Crear el producto
        const data = {
            ...body,
            name: body.name.toUpperCase(),
            user: req.userAuth._id,

        }

        const newProduct = new ProductModel(data);

        // Guardar el producto
        newProduct.save();

        // Enviar respuesta
        res.status(201).json({
            msg: 'Create Product',
            newProduct
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: ' Error en la peticion'
        })
    }
}

const updateProduct = async(req = request, res = response) => {

    const { id } = req.params;
    const { name, price, accessible, category, ...data } = req.body;


    if (typeof(accessible) !== 'boolean') {
        return res.status(400).json({
            msg: `accessible debe ser boolean`
        })
    }

    const newPrice = Number(price);

    const [product, confirName] = await Promise.all([
        ProductModel.findById(id),
        ProductModel.findOne({ name: name.toUpperCase() })
    ])


    const exitName = () => {
        if (name !== product.name && confirName == null) {
            return name.toUpperCase();
        } else {
            return product.name;
        }
    }

    if (!product) {
        res.status(401).json({
            msg: 'El producto no existe'
        });
    }

    // estado del producto
    if (!product.status) {
        res.status(401).json({
            msg: 'El producto no existe - status'
        })
    }


    // Crear la nueva data
    const newData = {
        name: exitName(),
        price: newPrice,
        accessible: (accessible) ? accessible : product.accessible
    }

    const updatePr = await ProductModel.findByIdAndUpdate(id, newData, {
        new: true
    })

    // Update

    res.json({
        msg: 'updateProduct',
        product,
        updatePr
    });
}

const deleteProduct = async(req = request, res = response) => {

    const { id } = req.params;

    const product = await ProductModel.findById(id);

    if (!product.status) {
        return res.status(401).json({
            msg: `El producto no existe`
        })
    }

    const deleteProduct = await ProductModel.findByIdAndUpdate(id, { status: false }, { new: true });

    res.json({
        msg: 'deleteProduct',
        deleteProduct
    });
}

module.exports = {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
}