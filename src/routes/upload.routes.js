const { Router } = require('express');
const { check } = require('express-validator');

const { uploadFile, uploadImgCloudinary, showImg } = require('../controllers');
const { validateFileUpload, validateContent, permittedExt } = require('../middlewares');
const { permittedCollections } = require('../helpers');

const router = Router();

// Crear la subida de un archivo normal
router.post('/', [
    validateFileUpload
], uploadFile);

// Actuliza la img de la coleccion ya creada
router.put('/:collection/:id', [
    validateFileUpload,
    permittedExt,
    check('id', 'No es in id de mongo').isMongoId(),
    check('collection').custom(c => permittedCollections(c, ['user', 'product'])),
    validateContent
], uploadImgCloudinary);


// Obtener la img de una coleccion
router.get('/:collection/:id', [
    check('id', 'No es in id de mongo').isMongoId(),
    check('collection').custom(c => permittedCollections(c, ['user', 'product'])),
    validateContent
], showImg);


module.exports = router;