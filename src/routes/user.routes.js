const { Router } = require('express');
const { check } = require('express-validator');

const { getUsers, getUserById, createUser, updateUser, deleteUser } = require('../controllers');
const { validateRole, existEmail, existUserById } = require('../helpers');
const { validateContent, validateJwt, hasRol } = require('../middlewares');

const router = Router();

// Obtener todos los usuarios paginados: desde - limit
router.get('/', getUsers);

// Obtener un usuario por Id
router.get('/:id', [
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existUserById),
    validateContent
], getUserById);

// Crear un usuario
router.post('/', [
    check('name', 'the name is required').not().isEmpty(),
    check('password', 'the password is required and have letter min 6').isLength({ min: 6 }),
    check('email', 'this don´t email').isEmail(),
    check('email').custom(existEmail),
    // check('role', 'this don´t role').isIn(['Admin_Role', 'User_Role'])
    check('rol').custom(validateRole),
    validateContent
], createUser);

// Actualizar usuario por Id: 
router.put('/:id', [
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existUserById),
    check('rol').custom(validateRole),
    validateContent
], updateUser);

// Eliminar un usuario por Id cambiar su estado: status:false
router.delete('/:id', [
    validateJwt,
    // isAdminRol,
    hasRol('ADMIN_ROLE', 'VENTAS_ROLE'),
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existUserById),
    validateContent
], deleteUser);


module.exports = router;