const { Router } = require('express');
const { check } = require('express-validator');
const { searchesDb } = require('../controllers');
const { validateContent } = require('../middlewares');

const router = Router();

router.get('/:collection/:term', [
    check('collection', 'This collection is required').not().isEmpty(),
    check('term', 'This term is required').not().isEmpty(),
    validateContent
], searchesDb);

module.exports = router;