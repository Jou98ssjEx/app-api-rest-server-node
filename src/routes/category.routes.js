const { Router } = require('express');
const { check } = require('express-validator');

const { getCategories, getCategoryById, createCategory, updateCategory, deleteCategory } = require('../controllers');
const { existCategoryById } = require('../helpers');
const { validateContent, validateJwt, hasRol } = require('../middlewares');

const router = Router();


// Get categories
router.get('/', getCategories);

// Get category for id
router.get('/:id', [
    // validateJwt,
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existCategoryById),
    validateContent
], getCategoryById);

// Create category token
router.post('/', [
    validateJwt,
    check('name', 'This name is required').not().isEmpty(),
    validateContent
], createCategory);

// Update categories token
router.put('/:id', [
    validateJwt,
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existCategoryById),
    check('name', 'this name is required').not().isEmpty(),
    validateContent
], updateCategory);

// Delete categories token-Admin
router.delete('/:id', [
    validateJwt,
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existCategoryById),
    hasRol('ADMIN_ROLE'),
    validateContent
], deleteCategory);


module.exports = router;